﻿using Indeed.Models.Workers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Indeed.DataAccess.Repositories.Implementation
{
    public class WorkerRepository : Repository<Worker>
    {
        public WorkerRepository(string jsonDataFileName) : base(jsonDataFileName)
        {
        }
        public async override Task<IEnumerable<Worker>> GetAll()
        {
            return Context.Workers;
        }
        public override Task<Worker> Get(string name)
        {
            throw new NotImplementedException();
        }
        public async override Task<Worker> Create(Worker item)
        {
            Context.Workers.Add(item);
            SaveContext();
            return item;
        }

       

        public override Task<Worker> Remove(Worker item)
        {
            throw new NotImplementedException();
        }

        public override Task<Worker> Update(Worker item)
        {
            throw new NotImplementedException();
        }

        
    }
}
