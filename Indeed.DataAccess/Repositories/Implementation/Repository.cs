﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Indeed.DataAccess.Repositories.Implementation
{
    public abstract partial class Repository<T> : IRepository<T> where T : class, new()
    {
        protected readonly string _jsonDataFileName;
        public abstract Task<IEnumerable<T>> GetAll();
        public abstract Task<T> Get(string name);
        public abstract Task<T> Create(T item);
        public abstract Task<T> Update(T item);
        public abstract Task<T> Remove(T item);

        protected Repository(string jsonDataFileName)
        {
            _jsonDataFileName = jsonDataFileName;
        }

        protected void SaveContext()
        {
            string json = JsonConvert.SerializeObject(new
            {
                Workers = Context.Workers,
                Options = Context.Options,
                Requests = Context.Requests
            });
            File.WriteAllText(_jsonDataFileName, json);
        }

        
    }
}
