﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Indeed.DataAccess.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity> Get(string name);
        Task<TEntity> Create(TEntity item);
        Task<TEntity> Update(TEntity item);
        Task<TEntity> Remove(TEntity item);

    }
}
