﻿using Indeed.Models.Options;
using Indeed.Models.Requests;
using Indeed.Models.Workers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Indeed.DataAccess
{
    public static class Context
    {
        public static bool isActive = false;

        public static List<Worker> Workers;
        public static List<Request> Requests;
        public static List<Options> Options;
    }
}
