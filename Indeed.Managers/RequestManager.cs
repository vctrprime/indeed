﻿using Indeed.DataAccess;
using Indeed.DataAccess.Repositories.Implementation;
using Indeed.Models.Workers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Indeed.Managers
{
    public static class RequestManager
    {
        public static void CheckRequests()
        {
            var context = new UnitOfWork();
            WorkerRepository repository = context.Repository<Worker>() as WorkerRepository;
            var workers = repository.GetAll().Result;
            var operators = workers.Where(w => w is Operator);
            Debug.WriteLine("CheckRequests!");
        }
    }
}
