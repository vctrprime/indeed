﻿using Indeed.Managers;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Indeed.Web.Infrastructure
{
    public class HostedService : IHostedService
    {
        private Timer _timer;

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(CheckRequests, null, 0, 5000);
            return Task.CompletedTask;
        }

        void CheckRequests(object state)
        {
            RequestManager.CheckRequests();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
    }
}
