﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Indeed.Web.Models;
using Indeed.DataAccess.Repositories.Implementation;
using Indeed.DataAccess;
using Indeed.Models.Workers;

namespace Indeed.Web.Controllers
{
    public class HomeController : Controller
    {
        protected readonly UnitOfWork Context;
        private readonly WorkerRepository _repository;
        
        public HomeController()
        {
            Context = new UnitOfWork();
            _repository = Context.Repository<Worker>() as WorkerRepository;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
