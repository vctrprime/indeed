﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Indeed.Models.Requests
{
    public class Request : BaseEntity
    {
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? TakenDate { get; set; }
        public DateTime? ExecutedDate { get; set; }
        public string Executor { get; set; }


        public TimeSpan? WatingTime
        {
            get
            {
                return TakenDate - CreatedDate;
            }
        }
        public TimeSpan? ExecutedTime
        {
            get
            {
                return ExecutedDate - TakenDate;
            }
        }
        public TimeSpan? SummaryTime
        {
            get
            {
                return ExecutedDate - CreatedDate;
            }
        }
    }
}
