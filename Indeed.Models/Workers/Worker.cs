﻿using Indeed.Models.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace Indeed.Models.Workers
{
    public class Worker : BaseEntity
    {
        
        public string Function { get; set; }
        public Request WorkRequest { get; set; }

        public Worker()
        {
            Function = this.GetType().Name;
        }

    }
}
